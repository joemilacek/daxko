provider "aws" {
  region = "us-east-2"
}

# daxko vpc

resource "aws_vpc" "daxko-vpc-dev" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "daxko_vpc"
    Env  = "dev"
  }
}

# dev private subnet in us-east-2a

resource "aws_subnet" "daxko-private-subnet-a-dev" {
  vpc_id            = aws_vpc.daxko-vpc-dev.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-2a"

  tags = {
    Name = "daxko-private-subnet-a"
    Env  = "dev"
  }
}

# dev public subnet in use-east-2a

resource "aws_subnet" "daxko-public-subnet-a-dev" {
  vpc_id                  = aws_vpc.daxko-vpc-dev.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "us-east-2a"
  map_public_ip_on_launch = true

  tags = {
    Name = "daxko-public-subnet-a"
    Env  = "dev"
  }
}

# internet gateway for dev vpc

resource "aws_internet_gateway" "daxko-internet-gateway-dev" {
  vpc_id = aws_vpc.daxko-vpc-dev.id

  tags = {
    Name = "daxko-gateway-dev"
    Env = "dev"
  }
}


# public route table for vpc

resource "aws_route_table" "daxko-useast2-public-route-table-dev" {
  vpc_id = aws_vpc.daxko-vpc-dev.id

  tags = {
    Name = "daxko-useast2-public-route-table-dev"
    Env = "dev"
  }
}

# default route for public routes

resource "aws_route" "daxko-useast2-public-route-default-dev" {
  route_table_id         = aws_route_table.daxko-useast2-public-route-table-dev.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.daxko-internet-gateway-dev.id
  depends_on             = [aws_route_table.daxko-useast2-public-route-table-dev]
}

# attach route table to public subnet a

resource "aws_route_table_association" "daxko-useast2-public-route-table-association-subnet-a-dev" {
  subnet_id      = aws_subnet.daxko-public-subnet-a-dev.id
  route_table_id = aws_route_table.daxko-useast2-public-route-table-dev.id
}


# default vpc acl
# blocks all ingress
# allows all egress

resource "aws_default_network_acl" "daxko-acl-dev" {
  default_network_acl_id = aws_vpc.daxko-vpc-dev.default_network_acl_id

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name = "daxko-acl-dev"
    Env = "dev"
  }
}


# get latest amazon linux ami to use for webserverdev1 instance

data "aws_ami" "amaz-linux" {
  owners      = ["amazon"]
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

# webserverdev1 ec2 instance
# install apache, create basic index.html

resource "aws_instance" "daxko-webserverdev1" {
  ami           = data.aws_ami.amaz-linux.id
  instance_type = "t2.micro"
  associate_public_ip_address = true
  key_name = "aws-jmilacek"
  # installs apache and creates basic webpage
  user_data     = <<-EOF
                  #!/bin/bash
                  sudo yum update httpd -y
                  sudo yum install httpd -y
                  sudo yum install mod_ssl -y                  
                  sudo systemctl enable httpd
                  sudo sh -c "echo 'Hello Daxko' >> /var/www/html/index.html"
                  sudo systemctl restart httpd
                  sudo groupadd daxko-devs
                  sudo useradd developer1
                  sudo usermod -a -G daxko-devs developer1
                  sudo su - developer1
                  mkdir .ssh
                  chmod 700 .ssh
                  touch .ssh/authorized_keys
                  chmod 600 .ssh/authorized_keys
                  EOF


  vpc_security_group_ids = [aws_security_group.daxko-webserver-security-group-dev.id]

  # create in public subnet-a
  subnet_id   = aws_subnet.daxko-public-subnet-a-dev.id

  tags = {
    Name = "webserverdev1"
    Env  = "dev"
    hostname = "webserverdev1"
  }
}

# security group for dev webserver

resource "aws_security_group" "daxko-webserver-security-group-dev" {
  name        = "daxko-webserver-security-group-dev"
  description = "dev webserver sec group"
  vpc_id      = aws_vpc.daxko-vpc-dev.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "daxko-webserver-security-group-dev"
    env  = "dev"
  }
}

# allow 80 to webserverdev1 from anywhere

resource "aws_security_group_rule" "daxko-webserver-security-group-rule-80-dev" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow all to TCP port 80"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.daxko-webserver-security-group-dev.id
  type              = "ingress"
}

# allow 443 to webserverdev1 from anywhere

resource "aws_security_group_rule" "daxko-webserver-security-group-rule-443-dev" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow all to TCP port 443"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.daxko-webserver-security-group-dev.id
  type              = "ingress"
}

# allow ssh to webserverdev1 from anywhere

resource "aws_security_group_rule" "daxko-webserver-security-group-rule-22-dev" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow all to TCP port 22"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  security_group_id = aws_security_group.daxko-webserver-security-group-dev.id
  type              = "ingress"
}


